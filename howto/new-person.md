---
title: How to get a new Tor System Administrator (with web developer duties) on board
---

Note that this documentation needs work, as it overlaps with normal
user management procedures, see [issue 40129](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40129).

# Glossary

 * TSA: Tor System Administrators
 * TPA: Tor Project Admins, synonymous with TSA, preferably used to
   disambiguate with [the other TSAs](https://en.wikipedia.org/wiki/TSA)
 * TPI: Tor Project Inc. the company that employs Tor staff
 * TPO: `torproject.org`, machines officially managed by TSA, often
   shortened as `.tpo`, for example. `www.tpo`
 * `torproject.net`, machines in DNS but not officially managed by TSA
 * a sysadmin can also be a service admin, and both can be paid work

# Orienteering

 * [sysadmin wiki](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/)
 * [service list](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service)
 * [machines list](https://db.torproject.org/machines.cgi)
 * key machines:
   * [jump host](/doc/ssh-jump-host) and "general shell server": `perdulce`
   * IRC idling host: `chives`
   * Puppet: `pauli`
   * LDAP: `alberti`
   * Nagios: `hetzner-hel1-01.torproject.org`
   * Main mail server: `eugeni`
   * Master Ganeti nodes: `fsn-node-01`, `chi-node-01`
 * key services:
   * [Grafana](howto/grafana): <https://grafana.torproject.org> - monitoring
     dashboard, password on `admin/tor-passwords.git`
   * [Nagios](howto/nagios):
     <https://nagios.torproject.org/cgi-bin/icinga/status.cgi?allunhandledproblems>
     legacy alert dashboard
   * [GitLab](howto/gitlab): <https://gitlab.torproject.org/> - issue tracking,
     project management, and git repository hosting
   * [git](howto/git): <https://gitweb.torproject.org/>, or
     `git@git-rw.torproject.org` over SSH - legacy git repository
     hosting
   * see also the full [service list](service)
 * key mailing lists:
   * <tor-project@lists.torproject.org> - Open list where anyone is welcome to watch but posting is moderated. Please favor using this when you can.
   * <tor-internal@lists.torproject.org> - If something truly can't include the wider community then this is the spot.
   * <tor-team@lists.torproject.org> - Exact same as tor-internal@ except that the list will accept email from non-members. If you need a cc when emailing a non-tor person then this is the place.
   * <tor-employees@lists.torproject.org> - TPI staff mailing list
   * <tor-meeting@lists.torproject.org> - for public meetings
   * <torproject-admin@torproject.org> - TPA-specific "mailing list"
     (not a mailing list but an alias)
 * IRC channels:
   * `#tor-project` - general Tor project channel
   * `#tpo-admin` - channel for TPA specific stuff
   * `#tor-www` - Tor websites development channel
   * `#tor-l10n` - Tor localization channel
   * `#tor-internal` - channel for private discussions, need secret
     password and being added to the `@tor-tpomember` with `GroupServ`,
     part of the `tor-internal@lists.tpo` welcome email)
   * `#tor-bots` - where a lot of bots live
   * `#tor-nagios` ... except the Nagios bot, which lives here
   * `#tor-meeting` - where some meetings are held
   * `#tor-meeting2` - fallback for the above
 * TPI stuff: see employee handbook from HR

# Important documentation

 1. [Getting to know LDAP](howto/ldap#getting-to-know-ldap)
 2. [SSH jump host configuration](doc/ssh-jump-host)
 3. [How to edit this wiki](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#editing-the-wiki-through-git), make sure you have a local copy of the
    documentation!
 4. [Puppet primer: adding yourself to the allow list](howto/puppet#adding-an-ip-address-to-the-global-allow-list)
 5. [New machine creation](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/new-machine)
 6. [Updating status.tpo](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/status#creating-new-issues)
 7. [Tor Websites](https://gitlab.torproject.org/tpo/web/team)

# More advanced documentation

 1. [Account creation procedures](howto/create-a-new-user)
 2. Password manager procedures (undocumented, see
    `ssh://git@git-rw.torproject.org/admin/tor-passwords.git` for now)
 3. [Adding](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component#adding-a-new-component) and [removing](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component#removing-a-component) websites in the [static mirror
    system](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component)
 4. [Editing DNS](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/dns#editing-a-zone)
 5. [TLS certificate operations](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/tls#how-to-get-an-x509-certificate-for-a-domain-with-lets-encrypt)
 6. [Puppet code linting](howto/puppet#validating-puppet-code) and the entire [Puppet operations manual](howto/puppet)
 7. [Backup restore procedures](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/backup)
 8. [Documentation design](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#design)
 9. [Ganeti operations manual](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/ganeti)

The full documentation is available in the wiki and particularly from
the [service list](service).

# Accounts to create

This section is specifically targeted at *existing* sysadmins, which
should follow this checklist to create the necessary accounts on all
core services. More services might be required if the new person is
part of other service teams, see the [service list](service) for the
exhaustive list.

The first few steps are part of the TPI onboarding process and might
already have been performed.

Here's a checklist that should be copy-pasted in a ticket:

 1. [ ] mailing lists (`tor-internal@` and others, see list above)
 2. [ ] [about/people](https://torproject.org/about/people) web page ([source code](https://gitlab.torproject.org/tpo/web/tpo/-/tree/master/content/about/people))
 3. [ ] GitLab `-admin` account
 4. [ ] GitLab `tpo/tpa` group membership
 5. [ ] GitLab `tpo/web` group membership
 6. [ ] [New LDAP account](howto/create-a-new-user)
 7. [ ] [puppet](howto/puppet) git repository access (how?)
 8. [ ] TPA password manager access (`admin/tor-passwords.git` in gitolite)
 9. [ ] [Nagios](howto/nagios) access, contact should be created in
    `ssh://git@git-rw.torproject.org/admin/tor-nagios`, password in
    `/etc/icinga/htpasswd.users` directly on the server
 10. [ ] Sunet cloud access (e.g. `Message-ID: <87bm1gb5wk.fsf@nordberg.se>`)
 11. [ ] [Nextcloud](https://nc.torproject.net) account with groups TPI and TPA.

Extra services we are not directly responsible for, but that TPA
staff may administer at some point:

 1. [ ] [BBB](howto/conference) access
 2. [ ] [RT](howto/rt#new-rt-admin)
 3. [ ] [blog](service/blog)
 4. [ ] [btcpayserver](service/BTCpayserver)
 5. [ ] [gitolite admin](howto/git)
 6. [ ] [schleuder](service/schleuder)
